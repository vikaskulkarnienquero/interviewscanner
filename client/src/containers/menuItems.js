const menuItems = {
  header: [
    {
      title: 'Registry',
      default: true,
      name: 'registry',
      iconClass: 'fa-th',

      children: [
        {
          title: 'Candidates',
          name: 'candidates',
          iconClass: 'fa-list-alt'
        }
      ]
    }
  ],
  title: { label: 'Scanner', class: 'fa fa-barcode' }
};

export default menuItems;
